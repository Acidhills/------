﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace DocumentSearch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        protected DocumentRepository _repo = new DocumentRepository();

        public ObservableCollection<Document> SearchResults;

        public MainWindow()
        {
            SearchResults = new ObservableCollection<Document>();
            DataContext = SearchResults;
            InitializeComponent();            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try{
                var doc = XDocument.Load("Products.xml");
                var items = doc.Root.Elements("item").Select(x => new Document
                {
                    Code = x.Attribute("Code").Value,
                    Product = x.Attribute("Product").Value,
                    Cost = double.Parse(x.Attribute("Cost").Value)
                });
                foreach(var item in items)
                {
                    _repo.Add(item);
                }
                MessageBox.Show("Данные загружены");

            }
            catch(Exception ex)
            {
                MessageBox.Show("Не удалось загрузить файл");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {

                _repo.Add(GetDoc());
            }
            catch 
            {
                MessageBox.Show("Неверные данные");
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                _repo.Delete(Code.Text);
            }
            catch
            {

            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            SearchResults.Clear();

            var res = _repo.GetById(GetDoc().Code);
            if (res != null)
              SearchResults.Add(res);

        }

        private Document GetDoc()
        {
            double temp;
            double.TryParse(Cost.Text, out temp);
            return new Document
            {
                Code = Code.Text,
                Product = Product.Text,
                Cost = temp
            };

        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            SearchResults.Clear();

            var res = _repo.GetByProduct (GetDoc().Product);
            if (res != null)
                foreach(var p in res)
                 SearchResults.Add(p);
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            SearchResults.Clear();

            var res = _repo.GetByCost(GetDoc().Cost);
            if (res != null)
                foreach (var p in res)
                    SearchResults.Add(p);
        }
    }
}
