﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Document
    {
        public string Code { get; set; }

        public string Product { get; set; }

        public double Cost { get; set; }
    }
}
