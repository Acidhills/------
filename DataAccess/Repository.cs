﻿using BTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DocumentRepository
    {
        private BTree<string, Document> _storage = new BTree<string, Document>(4);
        private BTree<string, Document> _productIndex = new BTree<string, Document>(4);
        private BTree<double, Document> _costIndex = new BTree<double, Document>(4);

        public void Add(Document entity)
        {
            if (GetById(entity.Code) != null)
                Delete(entity.Code);
            _storage.Insert(entity.Code, entity);
            _productIndex.Insert(entity.Product, entity);
            _costIndex.Insert(entity.Cost, entity);

        }

        public Document GetById(string id)
        {
            var res = _storage.Search(id);
            return res != null ? res.Pointer.FirstOrDefault() : null;
        }
        public List<Document> GetByProduct(string product)
        {
            var res = _productIndex.Search(product);
            return res != null ? res.Pointer : null;
        }
        public List<Document> GetByCost(double cost)
        {
            var res = _costIndex.Search(cost);
            return res != null ? res.Pointer : null;
        }

        public void Delete(string Code)            
        {
            var entity = GetById(Code);

            _storage.Delete(entity.Code);


            var costs = _costIndex.Search(entity.Cost);
            if (costs.Pointer.Count() > 0)
                costs.Pointer.Remove(costs.Pointer.First(x => x.Code == entity.Code));
            else
                _costIndex.Delete(entity.Cost);

            var products = _productIndex.Search(entity.Product);
            if (products.Pointer.Count() > 0)
                products.Pointer.Remove(products.Pointer.First(x => x.Product == entity.Product));
            else
                _productIndex.Delete(entity.Product);

        }


       

    }
}
